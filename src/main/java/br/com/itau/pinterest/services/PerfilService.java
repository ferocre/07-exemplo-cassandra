package br.com.itau.pinterest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.pinterest.api.PerfilEntrada;
import br.com.itau.pinterest.models.Login;
import br.com.itau.pinterest.models.Perfil;
import br.com.itau.pinterest.repositories.LoginRepository;
import br.com.itau.pinterest.repositories.PerfilRepository;

@Service
public class PerfilService {
	@Autowired
	PerfilRepository perfilRepository;

	@Autowired
	LoginRepository loginRepository;

	public void inserir(PerfilEntrada perfilEntrada) {
		inserirPerfil(perfilEntrada);
		inserirLogin(perfilEntrada);
	}

	private void inserirPerfil(PerfilEntrada perfilEntrada) {
		Perfil perfil = new Perfil();

		perfil.setNome(perfilEntrada.getNome());
		perfil.setEmail(perfilEntrada.getEmail());
		perfil.setSexo(perfilEntrada.getSexo());
		perfil.setDataNascimento(perfilEntrada.getDataNascimento());
		perfil.setPreferencias(perfilEntrada.getPreferencias());
		
		perfilRepository.save(perfil);
	}
	
	private void inserirLogin(PerfilEntrada perfilEntrada) {
		Login login = new Login();
		
		login.setEmail(perfilEntrada.getEmail());
		login.setSenha(perfilEntrada.getSenha());
		
		loginRepository.save(login);
	}
}
