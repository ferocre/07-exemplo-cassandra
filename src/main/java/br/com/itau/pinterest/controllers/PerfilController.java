package br.com.itau.pinterest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.pinterest.api.PerfilEntrada;
import br.com.itau.pinterest.services.PerfilService;

@RestController
@RequestMapping("/perfil")
public class PerfilController {
	@Autowired
	PerfilService perfilService;
	
	@PostMapping
	public ResponseEntity criar(@RequestBody PerfilEntrada perfilEntrada) {
		perfilService.inserir(perfilEntrada);
		
		return ResponseEntity.status(201).build();
	}
}
